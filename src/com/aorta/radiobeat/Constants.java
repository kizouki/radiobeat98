package com.aorta.radiobeat;

public final class Constants {

	public enum FontType { FUTURA_STD_BOLD_OBLIQUE, FUTURA_STD_BOOK }
	
	public final class Api {

		public static final String _HOST = "http://api.playme.com.br";
		
		public static final String _HOST_MEDIA = "http://media.playme.com.br";

		public static final String ANDROID_DEVICE_MODEL_ID = "888f3397-0659-43bd-817c-c357e1a3f934";

		public static final String BASE_URL = _HOST + "/mobile/";
		
		public static final String BASE_URL_INTERATIVIDADE = _HOST + "/interatividade/";

		public static final String DEFAULT_NAMESPACE = "http://tempuri.org/";

		public static final String IMAGE_DOWNLOAD_URL = _HOST_MEDIA + "/download.ashx?wimg=%s&himg=%s&qimg=%s&ext=%s&idarq=%s";
	}

	public final class Cache {

		public static final String CACHE_FILES_PREFIX = "adhoc_cache";
	}

	public final class SharedPrefsKeys {

		public static final String AGREEMENT = "agreement";
		
		public static final String APP_ID = "app_id";
		
		public static final String DEFAULT_SHARED_PREFS = "default_shared_prefs";
		
		public static final String TWITTER_TOKEN = "twitter_token";
		
		public static final String TWITTER_SECRET = "twitter_secret";		
	}
	
	public final class Channel { 
		
		public static final String ID = "0ae21c6a-2c94-419c-af16-eab146a593fd";
	}
	
	public final class Content { 
		
		public static final String LIKE_PARAM = "1";
		public static final String DISLIKE_PARAM = "-1";
		
		public static final String MUSIC_INFO_URL = "http://api.playme.com.br/radiobeat98service/AoVivo.ashx";
		
		public static final String LOVE_HATE_URL = "http://api.playme.com.br/radiobeat98service/AoVivoInteratividade.ashx?"+
				"curtir=%s&pulsarId=%s&grupoMidiaId=%s&nomeArtista=%s&nomeMusica=%s";
	}
	
	public final class Advertising {
		
		public static final int DELAY = 1000 * 5;
		
		public static final String SPLASH = "http://ads.globo.com/RealMedia/ads/adstream_sx.ads/beat98android/splash/%s@Position1 ";
		
		public static final String PLAYER = "http://ads.globo.com/RealMedia/ads/adstream_sx.ads/beat98android/player/%s@Position3 ";
	}
	
	public final class Twitter {
		
		public static final String CONSUMER_KEY = "wTaMS4YKW7k8R5Z3XXOpQ";
		
		public static final String CONSUMER_SECRET = "qAYFnndlMTH7HKDh3kSYmMK8kcAwwbYYI7rEG95cGc";		
	}
	
	public final class Facebook {
		
		public static final String APP_KEY = "224325231031599";
	}
	
	public static final String CUSTOMER_ID = "57";	
}