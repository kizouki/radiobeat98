package com.aorta.radiobeat.player;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.spoledge.aacplayer.ArrayAACPlayer;
import com.spoledge.aacplayer.ArrayDecoder;
import com.spoledge.aacplayer.Decoder;
import com.spoledge.aacplayer.PlayerCallback;

public class AACPlayerService extends Service {
	
	public static final String AAC_PLAYER_CURRENT_STATE = "aac_player_current_state";
	public static final String AAC_PLAYER_STATE_CHANGE = "com.aorta.radiobeat.player.statechange";
	
	public enum PlayerState { STOPPED, PLAYING, ERROR, NOTHING, STARTING }
	
	private PhoneStateListener listener;
	private boolean isPausedInCall = false;
	private TelephonyManager telephonyManager;
	
	private ArrayAACPlayer player;
	private PlayerState playerState;
	
	private String streamUrl;
	
	private int bindCount = 0;
	
	@Override
	public void onCreate() {
		
		initPlayer();
		
		initTelephonyManager();
	}
	
	@Override
	public void onDestroy() {
		
		super.onDestroy();

		stop();
		
		synchronized (this) {
			
			if (player != null) {

				player = null;
			}
		}

		telephonyManager.listen(listener, PhoneStateListener.LISTEN_NONE);
	}	

	@Override
	public IBinder onBind(Intent intent) {

		bindCount++;
		
		return new ListenBinder();
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		
		bindCount--;

		if (!isPlaying() && bindCount == 0) {
			
			stopSelf();
		}
			
		return false;
	}	
	
	private void initTelephonyManager() {
		
		telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		
		listener = new PhoneStateListener() {
			
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				
				switch (state) {
				
					case TelephonyManager.CALL_STATE_OFFHOOK:
						
					case TelephonyManager.CALL_STATE_RINGING:
						
						if (isPlaying()) {
							
							stop();
							
							isPausedInCall = true;
						}
						
						break;
						
					case TelephonyManager.CALL_STATE_IDLE:
						
						if (isPausedInCall) {
							
							play(AACPlayerService.this.streamUrl);
							
							isPausedInCall = false;
						}
						
						break;
				}
			}
		};
		
		telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);		
	}
	
	private void initPlayer() {
		
		playerState = PlayerState.NOTHING;
		
		player = new ArrayAACPlayer(ArrayDecoder.create(Decoder.DECODER_OPENCORE));
		
		player.setPlayerCallback(new PlayerCallback() {
			
			@Override
			public void playerException(Throwable throwable) { 
				
				player.stop();

				playerState = PlayerState.ERROR;
				
				sendStateChangeBroadcast();
			}

			@Override
			public void playerPCMFeedBuffer(boolean arg0, int arg1, int arg2) { }

			@Override
			public void playerStarted() { 
							
				playerState = PlayerState.PLAYING;
				
				sendStateChangeBroadcast();
			}

			@Override
			public void playerStopped(int arg0) { 
				
				playerState = PlayerState.STOPPED;
				
				sendStateChangeBroadcast();
			}
		});		
	}
	
	synchronized public boolean isPlaying() {
		
		return playerState == PlayerState.PLAYING;
	}	
	
	synchronized public void play(String streamUrl) {
		
		if (!isPlaying()) {
			
			this.streamUrl = streamUrl;

			try {
				
				playerState = PlayerState.STARTING;
				
				sendStateChangeBroadcast();
				
				player.playAsync(streamUrl);
				
			} catch (Exception e) {

				e.printStackTrace();
				
				playerState = PlayerState.ERROR;
				
				sendStateChangeBroadcast();
			}
		}
	}	
	
	synchronized public void stop() {
		
		if (isPlaying()) {

			player.stop();
		}
	}	
	
	protected void sendStateChangeBroadcast() {
		
		Intent stateChangeBroadcast = new Intent(AAC_PLAYER_STATE_CHANGE);
		
		stateChangeBroadcast.putExtra(AAC_PLAYER_CURRENT_STATE, playerState.toString());
		
		getApplicationContext().sendBroadcast(stateChangeBroadcast);
	}	
	
	public class ListenBinder extends Binder {
		
		public AACPlayerService getService() {
			
			return AACPlayerService.this;
		}		
	}	
}