package com.aorta.radiobeat.api;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import com.aorta.radiobeat.Constants;
import com.aorta.radiobeat.api.to.Channel;
import com.aorta.radiobeat.helper.ApiHelper;

public class ChannelApi {

	private static final String ACTION = Constants.Api.DEFAULT_NAMESPACE + "ObterCanalJSON";

	private static final String METHOD = "ObterCanalJSON";

	private static final String URL = Constants.Api.BASE_URL + "ServicosCanal.asmx";

	public static Channel getByChannel(String channelId) throws Exception {

		SoapObject soapRoot = new SoapObject(Constants.Api.DEFAULT_NAMESPACE, METHOD);

		PropertyInfo pi = new PropertyInfo();
		pi.setName("IdCanal");
		pi.setValue(channelId);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);

		SoapPrimitive content = (SoapPrimitive) ApiHelper.request(ACTION, URL, soapRoot);

		String json = content.toString();

		Channel channel = Channel.createByJson(json, Channel.class);
		
		return channel;
	}
}