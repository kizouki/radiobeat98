package com.aorta.radiobeat.api.to;

import java.util.List;

import com.google.gson.Gson;

public class Channel extends To {
	
    private int CodigoRetorno;
	private String DescricaoRetorno;
	private boolean OcorreuErro;
	private String Id;
	private String IdRadio;
	private String Idarquivo;
	private List<Channel> Lista;
	private String Nome;
	private String StreamUrl;
	private String Url;
	
	public Channel() { }
	
	public static Channel createByJson(String json) {
		
		Gson gson = new Gson();
		
		return gson.fromJson(json, Channel.class);
	}

	public int getCodigoRetorno() {
		return CodigoRetorno;
	}

	public void setCodigoRetorno(int codigoRetorno) {
		CodigoRetorno = codigoRetorno;
	}

	public String getDescricaoRetorno() {
		return DescricaoRetorno;
	}

	public void setDescricaoRetorno(String descricaoRetorno) {
		DescricaoRetorno = descricaoRetorno;
	}

	public boolean getOcorreuErro() {
		return OcorreuErro;
	}

	public void setOcorreuErro(boolean ocorreuErro) {
		OcorreuErro = ocorreuErro;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getIdRadio() {
		return IdRadio;
	}

	public void setIdRadio(String idRadio) {
		IdRadio = idRadio;
	}

	public String getIdarquivo() {
		return Idarquivo;
	}

	public void setIdarquivo(String idarquivo) {
		Idarquivo = idarquivo;
	}

	public List<Channel> getLista() {
		return Lista;
	}

	public void setLista(List<Channel> lista) {
		Lista = lista;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public String getStreamUrl() {
		return StreamUrl;
	}

	public void setStreamUrl(String streamUrl) {
		StreamUrl = streamUrl;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}
}