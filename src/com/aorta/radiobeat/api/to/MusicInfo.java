package com.aorta.radiobeat.api.to;

public class MusicInfo extends To {

	private String artista;
	private String titulo;
	private String album;
	private String imagem;
	private String data;
	private Lyric letra;
	private Interactivity interatividade;
	
	public MusicInfo() { }
	
	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Lyric getLetra() {
		return letra;
	}

	public void setLetra(Lyric letra) {
		this.letra = letra;
	}

	public Interactivity getInteratividade() {
		return interatividade;
	}

	public void setInteratividade(Interactivity interatividade) {
		this.interatividade = interatividade;
	}
	
	public static class Lyric {
		
		private String nativa;
		private String traduzida;
		
		public Lyric() { }

		public String getNativa() {
			return nativa;
		}

		public void setNativa(String nativa) {
			this.nativa = nativa;
		}

		public String getTraduzida() {
			return traduzida;
		}

		public void setTraduzida(String traduzida) {
			this.traduzida = traduzida;
		}
	}
	
	public static class Interactivity {
		
		private int curtir;
		private int origemId;
		private String grupoMidiaId;
		private String pulsarId;
		private String nomeArtista;
		private String nomeMusica;
		
		public Interactivity() { }

		public int getCurtir() {
			return curtir;
		}

		public void setCurtir(int curtir) {
			this.curtir = curtir;
		}

		public int getOrigemId() {
			return origemId;
		}

		public void setOrigemId(int origemId) {
			this.origemId = origemId;
		}

		public String getGrupoMidiaId() {
			return grupoMidiaId;
		}

		public void setGrupoMidiaId(String grupoMidiaId) {
			this.grupoMidiaId = grupoMidiaId;
		}

		public String getPulsarId() {
			return pulsarId;
		}

		public void setPulsarId(String pulsarId) {
			this.pulsarId = pulsarId;
		}

		public String getNomeArtista() {
			return nomeArtista;
		}

		public void setNomeArtista(String nomeArtista) {
			this.nomeArtista = nomeArtista;
		}

		public String getNomeMusica() {
			return nomeMusica;
		}

		public void setNomeMusica(String nomeMusica) {
			this.nomeMusica = nomeMusica;
		}		
	}
}