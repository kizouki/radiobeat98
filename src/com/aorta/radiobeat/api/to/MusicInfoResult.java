package com.aorta.radiobeat.api.to;

public class MusicInfoResult extends To {

	private MusicInfo musicaCorrente;
	private MusicInfo musicaPosterior;
	
	public MusicInfoResult() { }

	public MusicInfo getMusicaCorrente() {
		return musicaCorrente;
	}

	public void setMusicaCorrente(MusicInfo musicaCorrente) {
		this.musicaCorrente = musicaCorrente;
	}

	public MusicInfo getMusicaPosterior() {
		return musicaPosterior;
	}

	public void setMusicaPosterior(MusicInfo musicaPosterior) {
		this.musicaPosterior = musicaPosterior;
	}	
}