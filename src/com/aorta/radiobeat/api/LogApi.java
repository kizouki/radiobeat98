package com.aorta.radiobeat.api;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import com.aorta.radiobeat.Constants;
import com.aorta.radiobeat.helper.ApiHelper;

public class LogApi {

	private static final String INSERT_ACTION =	Constants.Api.DEFAULT_NAMESPACE + "InserirLogAcessoAplicativo";

	private static final String INSERT_METHOD = "InserirLogAcessoAplicativo";

	private static final String UPDATE_ACTION =	Constants.Api.DEFAULT_NAMESPACE + "AtualizarLogAcessoAplicativo";

	private static final String UPDATE_METHOD = "AtualizarLogAcessoAplicativo";
	
	private static final String INSERT_ACTION_CHANNEL =	Constants.Api.DEFAULT_NAMESPACE + "InserirLogAcesso";

	private static final String INSERT_METHOD_CHANNEL = "InserirLogAcesso";	
	
	private static final String UPDATE_ACTION_CHANNEL =	Constants.Api.DEFAULT_NAMESPACE + "AtualizarLogAcesso";

	private static final String UPDATE_METHOD_CHANNEL = "AtualizarLogAcesso";	

	private static final String URL = Constants.Api.BASE_URL + "ServicosEstatistica.asmx";

	public static final String beginAppLog(String appId) throws Exception {

		SoapObject soapRoot = new SoapObject(Constants.Api.DEFAULT_NAMESPACE, INSERT_METHOD);

		PropertyInfo pi;

		pi = new PropertyInfo();
		pi.setName("idUsuario");
		pi.setValue(appId);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);

		pi = new PropertyInfo();
		pi.setName("idCliente");
		pi.setValue(Constants.CUSTOMER_ID);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);

		pi = new PropertyInfo();
		pi.setName("idDispositivo");
		pi.setValue(Constants.Api.ANDROID_DEVICE_MODEL_ID);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);

		SoapPrimitive content = (SoapPrimitive) ApiHelper.request(INSERT_ACTION, URL, soapRoot);

		String id = content.toString();

		return id;
	}

	public static final void endAppLog(String logId) throws Exception {

		SoapObject soapRoot = new SoapObject(Constants.Api.DEFAULT_NAMESPACE, UPDATE_METHOD);

		PropertyInfo pi;

		pi = new PropertyInfo();
		pi.setName("id");
		pi.setValue(logId);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);
		
		ApiHelper.request(UPDATE_ACTION, URL, soapRoot);
	}
	
	public static final String beginChannelLog(String appId, String canalId) throws Exception {

		SoapObject soapRoot = new SoapObject(Constants.Api.DEFAULT_NAMESPACE, INSERT_METHOD_CHANNEL);

		PropertyInfo pi;

		pi = new PropertyInfo();
		pi.setName("idUsuario");
		pi.setValue(appId);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);

		pi = new PropertyInfo();
		pi.setName("idCanal");
		pi.setValue(canalId);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);

		pi = new PropertyInfo();
		pi.setName("idDispositivo");
		pi.setValue(Constants.Api.ANDROID_DEVICE_MODEL_ID);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);

		SoapPrimitive content = (SoapPrimitive) ApiHelper.request(INSERT_ACTION_CHANNEL, URL, soapRoot);

		String id = content.toString();

		return id;
	}	
	
	public static final void endChannelLog(String logId) throws Exception {

		SoapObject soapRoot = new SoapObject(Constants.Api.DEFAULT_NAMESPACE, UPDATE_METHOD_CHANNEL);

		PropertyInfo pi;

		pi = new PropertyInfo();
		pi.setName("id");
		pi.setValue(logId);
		pi.setType(PropertyInfo.STRING_CLASS);

		soapRoot.addProperty(pi);
		
		ApiHelper.request(UPDATE_ACTION_CHANNEL, URL, soapRoot);
	}	
}