package com.aorta.radiobeat.api.task;

import java.net.URL;
import java.net.URLConnection;

import net.chilicat.m3u8.PlayList;

import com.aorta.radiobeat.api.to.To;

public class GetM3U8PlaylistTask extends TaskBase {

	public GetM3U8PlaylistTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {

		URL u = new URL(params[0]);

		URLConnection urlConnection = u.openConnection();
		
		PlayList playList = PlayList.parse(urlConnection.getInputStream());
		
		taskResponse.setTo(new To(playList));
	}
}