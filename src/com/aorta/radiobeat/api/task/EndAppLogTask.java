package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.LogApi;

public class EndAppLogTask extends TaskBase {

	public EndAppLogTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {

		String logId = params[0];
		
		LogApi.endAppLog(logId);
	}
}