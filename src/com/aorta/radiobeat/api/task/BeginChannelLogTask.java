package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.LogApi;
import com.aorta.radiobeat.api.to.To;

public class BeginChannelLogTask extends TaskBase {

	public BeginChannelLogTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {
		
		String appId = params[0];
		String canalId = params[1];
		
		String logId = LogApi.beginChannelLog(appId, canalId);
		
		taskResponse.setTo(new To(logId));
	}
}