package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.LogApi;
import com.aorta.radiobeat.api.to.To;

public class BeginAppLogTask extends TaskBase {

	public BeginAppLogTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {

		String appId = params[0];
		
		String logId = LogApi.beginAppLog(appId);
		
		taskResponse.setTo(new To(logId));
	}
}