package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.MusicApi;
import com.aorta.radiobeat.api.to.MusicInfoResult;

public class GetMusicInfoTask extends TaskBase {

	public GetMusicInfoTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {

		MusicInfoResult result = MusicApi.getMusicInfo();
		
		taskResponse.setTo(result);
	}
}