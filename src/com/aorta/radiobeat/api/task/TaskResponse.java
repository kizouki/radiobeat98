package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.to.To;

public final class TaskResponse {

	private To to;
	private Exception exception;
	
	public TaskResponse() { }

	public To getTo() {
		return to;
	}

	public void setTo(To to) {
		this.to = to;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}	
}