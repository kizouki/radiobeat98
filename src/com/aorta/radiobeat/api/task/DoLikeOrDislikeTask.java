package com.aorta.radiobeat.api.task;

import android.text.TextUtils;

import com.aorta.radiobeat.api.MusicApi;

public class DoLikeOrDislikeTask extends TaskBase {

	public DoLikeOrDislikeTask(TaskResult callBack) {
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {

		String likeOrDislike = !TextUtils.isEmpty(params[0]) ? params[0] : "";
		String pulsarId = !TextUtils.isEmpty(params[1]) ? params[1] : "";
		String grupoMidiaId = !TextUtils.isEmpty(params[2]) ? params[2] : "";
		String nomeArtista = !TextUtils.isEmpty(params[3]) ? params[3] : "";
		String nomeMusica = !TextUtils.isEmpty(params[4]) ? params[4] : "";
		
		MusicApi.doLikeOrDislike(likeOrDislike, pulsarId, grupoMidiaId, nomeArtista, nomeMusica);
	}
}