package com.aorta.radiobeat.api.task;

public class TwitterUpdateStatusTaskResponse {
	
	private Exception exception;
	private twitter4j.Status status;
	
	public TwitterUpdateStatusTaskResponse() { }

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public twitter4j.Status getStatus() {
		return status;
	}

	public void setStatus(twitter4j.Status status) {
		this.status = status;
	}
}