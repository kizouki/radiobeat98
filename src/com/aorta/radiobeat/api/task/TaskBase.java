package com.aorta.radiobeat.api.task;

import android.accounts.NetworkErrorException;
import android.os.AsyncTask;

import com.aorta.radiobeat.R;
import com.aorta.radiobeat.RBApplication;
import com.aorta.radiobeat.helper.NetworkHelper;

public abstract class TaskBase extends AsyncTask<String, Void, TaskResponse> {

	private TaskResult callBack;
	
	public TaskBase(TaskResult callBack) {
		
		this.callBack = callBack;
	}
	
	protected TaskResult getCallBack() {
		
		return callBack;
	}
	
	protected abstract void executeAsync(TaskResponse taskResponse, String... params) throws Exception;
	
	@Override
	protected final TaskResponse doInBackground(String... params) {

		TaskResponse taskResponse = new TaskResponse();
		
		try {
			
			if (!NetworkHelper.isOnline(RBApplication.getInstance()))
				throw new NetworkErrorException(RBApplication.getInstance().getString(R.string.network_error));
			
			executeAsync(taskResponse, params);
			
		} catch (Exception e) {
			
			taskResponse.setException(e);
			
			e.printStackTrace();
		}
		
		return taskResponse;
	}	
	
	@Override
	protected void onPostExecute(TaskResponse result) {

		if (!isCancelled() && callBack != null) {
			
			callBack.onTaskResult(this, result);
		}
	}
}