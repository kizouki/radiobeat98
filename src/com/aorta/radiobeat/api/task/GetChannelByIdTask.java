package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.ChannelApi;
import com.aorta.radiobeat.api.to.Channel;

public class GetChannelByIdTask extends TaskBase {

	public GetChannelByIdTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {
		
		String channelId = params[0];
		
		Channel channel = ChannelApi.getByChannel(channelId);
		
		taskResponse.setTo(channel);
	}
}