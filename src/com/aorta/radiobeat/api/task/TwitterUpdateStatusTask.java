package com.aorta.radiobeat.api.task;

import android.os.AsyncTask;

import com.aorta.radiobeat.RBApplication;

public class TwitterUpdateStatusTask extends AsyncTask<String, Void, TwitterUpdateStatusTaskResponse> {

	@Override
	protected TwitterUpdateStatusTaskResponse doInBackground(String... params) {

		TwitterUpdateStatusTaskResponse response = new TwitterUpdateStatusTaskResponse();
		
		try {
			
			response.setStatus(RBApplication.getInstance().getTwitter().updateStatus(params[0]));
			
		} catch (Exception e) {

			response.setException(e);
			
			e.printStackTrace();
		}
		
		return response;
	}
}