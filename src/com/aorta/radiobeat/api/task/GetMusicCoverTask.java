package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.MusicApi;
import com.aorta.radiobeat.api.to.To;

public class GetMusicCoverTask extends TaskBase {

	public GetMusicCoverTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {

		String url = params[0];
		
		byte[] musicCover = MusicApi.getMusicCover(url);
		
		taskResponse.setTo(new To(musicCover));
	}
}