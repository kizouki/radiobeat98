package com.aorta.radiobeat.api.task;

import com.aorta.radiobeat.api.LogApi;

public class EndChannelLogTask extends TaskBase {

	public EndChannelLogTask(TaskResult callBack) {
		
		super(callBack);
	}

	@Override
	protected void executeAsync(TaskResponse taskResponse, String... params) throws Exception {
		
		String logId = params[0];
		
		LogApi.endChannelLog(logId);
	}
}