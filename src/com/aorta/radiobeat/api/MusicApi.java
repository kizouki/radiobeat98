package com.aorta.radiobeat.api;

import com.aorta.radiobeat.Constants;
import com.aorta.radiobeat.api.to.MusicInfoResult;
import com.aorta.radiobeat.helper.HttpHelper;

public final class MusicApi {

	public static MusicInfoResult getMusicInfo() {
		
		String url = Constants.Content.MUSIC_INFO_URL;
		
		byte[] data = HttpHelper.request(url);
		
		if (data != null) {
			
			String json = new String(data);
			
			return MusicInfoResult.createByJson(json, MusicInfoResult.class);
		}
		
		return null;
	}
	
	public static byte[] getMusicCover(String url) {
		
		return HttpHelper.request(url);		
	}
	
	public static String doLikeOrDislike(String likeOrDislike, String pulsarId, String grupoMidiaId, String nomeArtista, String nomeMusica) {
		
		String url = String.format(Constants.Content.LOVE_HATE_URL, likeOrDislike, pulsarId, grupoMidiaId, nomeArtista, nomeMusica);
		
		url = url.replace(" ", "%20");
		
		byte[] data = HttpHelper.request(url);
		
		if (data != null) {
			
			String json = new String(data);
			
			return json;
		}
		
		return null;
	}
}