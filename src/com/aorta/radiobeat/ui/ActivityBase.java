package com.aorta.radiobeat.ui;

import java.util.UUID;

import android.accounts.NetworkErrorException;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.aorta.radiobeat.Constants;
import com.aorta.radiobeat.R;
import com.aorta.radiobeat.api.task.BeginAppLogTask;
import com.aorta.radiobeat.api.task.EndAppLogTask;
import com.aorta.radiobeat.api.task.TaskResponse;
import com.aorta.radiobeat.api.task.TaskResult;
import com.aorta.radiobeat.helper.SharedPreferencesHelper;

public abstract class ActivityBase extends SherlockFragmentActivity {
	
	public static final int NO_ERROR = 0;
	public static final int NETWORK_ERROR = 1;
	public static final int SERVER_ERROR = 2;
	
	private static int activityCount = 0;
	
	private Handler handler;
	private WaitDialog waitDialog;
	protected static String appLogId = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		handler = new Handler();
		
		appLogIn();
		
		activityCount++;
	}
	
	@Override
	protected void onDestroy() {

		try {
			
			hideWaitDialog();
			
		} finally {
			
			activityCount--;
			
			if (activityCount == 0) {
				
				appLogOut();
			}
			
			super.onDestroy();
		}
	}
	
	protected Handler getHandler() {
		
		return handler;
	}
	
	protected String getAppId() {

		String id = null;

		try {

			id = SharedPreferencesHelper.read(this, Constants.SharedPrefsKeys.DEFAULT_SHARED_PREFS, Constants.SharedPrefsKeys.APP_ID, null);

			if (id == null) {
				
				id = UUID.randomUUID().toString();
			}

			SharedPreferencesHelper.write(this, Constants.SharedPrefsKeys.DEFAULT_SHARED_PREFS, Constants.SharedPrefsKeys.APP_ID, id);

		} catch (Exception e) {
			
			id = null;
			e.printStackTrace();
		}

		return id;
	}	
	
	protected void hideWaitDialog() {

		if (waitDialog != null) {

			try {

				waitDialog.dismiss();

			} catch (Exception e) {

				e.printStackTrace();

			} finally {

				waitDialog = null;
			}
		}
	}
	
	protected void showWaitDialog(final AsyncTask<?, ?, ?> task) {

		showWaitDialog(task, R.layout.wait_dialog);
	}

	protected void showWaitDialog(final AsyncTask<?, ?, ?> task, final int layoutId) {

		hideWaitDialog();

		waitDialog = new WaitDialog(this, task != null, new OnCancelListener() {
			
			public void onCancel(DialogInterface dialog) {
				
				try {

					if (task != null) {
						
						task.cancel(false);
						
						onWaitDialogCanceled(task);
					}

				} catch (Exception e) {
					
					e.printStackTrace();
				}
			}

		}) {
			
			@Override
			protected int getLayoutId() {
				
				return layoutId;
			}
			
		};

		waitDialog.show();
	}	
	
	protected void onWaitDialogCanceled(final AsyncTask<?, ?, ?> task) {
		
		hideWaitDialog();
	}
	
	protected void onAppLogInResult(int error) { }
	
	private void appLogIn() {
		
		if (activityCount == 0) {
			
			String appId = getAppId();
			
			showWaitDialog(null, R.layout.wait_dialog_splash);
			
			new BeginAppLogTask(new TaskResult() {
				
				@Override
				public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {
					
					hideWaitDialog();

					if (taskResponse.getTo() != null) {
						
						appLogId = taskResponse.getTo().getTag().toString();
						
						onAppLogInResult(NO_ERROR);
						
					} else if (taskResponse.getException() != null) {
						
						Exception e = taskResponse.getException();
						
						if (e instanceof NetworkErrorException)
							onAppLogInResult(NETWORK_ERROR);
						else
							onAppLogInResult(SERVER_ERROR);
					}
				}
				
			}).execute(appId);			
		}
	}	
	
	private void appLogOut() {
		
		if (appLogId != null) {
			
			new EndAppLogTask(new TaskResult() {
				
				@Override
				public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) { }
				
			}).execute(appLogId);			
		}
	}
	
	private void showMessage(String title, String message, String positiveBtn,
			final Runnable positiveCallback, String negativeBtn, final Runnable negativeCallback) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		builder.setTitle(title);
		
		builder.setMessage(message);
		
		builder.setCancelable(false);
		
		builder.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (positiveCallback != null) {
					
					positiveCallback.run();
				}	
			}
		});
		
		builder.setNegativeButton(negativeBtn, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (negativeCallback != null) {
					
					negativeCallback.run();
				}
			}
		});
		
		hideWaitDialog();
		
		builder.show();
	}
	
	protected void showInformation(String message, Runnable callback) {
		
		showMessage(getString(R.string.app_name), message, getString(R.string.dialog_ok), callback, null, null);
	}
	
	protected void showConfirmation(String message, String positiveBtn, final Runnable positiveCallback,
			String negativeBtn, final Runnable negativeCallback) {
		
		showMessage(getString(R.string.app_name), message, positiveBtn, positiveCallback, negativeBtn, negativeCallback);
	}
	
	protected int getNonce() {

		int nonce = (int) (System.currentTimeMillis() / 1000);
		
		return nonce;
	}
	
	protected void disableWebViewScrolls(WebView webView) {
		
		webView.setVerticalScrollBarEnabled(false);
		webView.setHorizontalScrollBarEnabled(false);

		webView.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				return (event.getAction() == MotionEvent.ACTION_MOVE);
			}
		});		
	}	
	
	protected class LocalViewClient extends WebViewClient {

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) { }

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

			return true;
		}
	}	
}