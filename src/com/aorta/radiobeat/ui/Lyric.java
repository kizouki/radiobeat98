package com.aorta.radiobeat.ui;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.aorta.radiobeat.Constants.FontType;
import com.aorta.radiobeat.R;
import com.aorta.radiobeat.RBApplication;
import com.aorta.radiobeat.api.to.MusicInfoResult;

public class Lyric extends ActivityBase {

	private ActionBar actionBar;
	
	private TextView txtMusicName;
	private TextView txtMusicaArtist;
	private TextView txtSongWriter;
	private WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.lyric);
		
		actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	    actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header));
	    actionBar.setDisplayShowTitleEnabled(false);
	    actionBar.setDisplayHomeAsUpEnabled(false);	
	    
	    initViews();
	    
	    updateViews();
	}
	
	private void initViews() {
		
		txtMusicName = (TextView) findViewById(R.id.lyric_music_name);
		txtMusicaArtist = (TextView) findViewById(R.id.lyric_music_artist);
		txtSongWriter = (TextView) findViewById(R.id.lyric_song_writer);
		webView = (WebView) findViewById(R.id.lyric_webview);
		
		txtMusicName.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOLD_OBLIQUE));
		txtMusicaArtist.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOLD_OBLIQUE));
		txtSongWriter.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOLD_OBLIQUE));
		
		webView.setBackgroundColor(0x00000000);
		webView.getSettings().setDefaultFontSize(12);
		
		showWaitDialog(null);
	}
	
	private void updateViews() {
		
		String json = getIntent().getExtras().getString("CURRENT_MUSIC_INFO_PARAM");
		
		MusicInfoResult info = MusicInfoResult.createByJson(json, MusicInfoResult.class);
		
		txtMusicName.setText(info.getMusicaCorrente().getTitulo());
		
		txtMusicaArtist.setText(info.getMusicaCorrente().getArtista());
		
		webView.loadDataWithBaseURL(null, info.getMusicaCorrente().getLetra().getNativa(), "text/html", "utf-8", "about:blank");
		
		hideWaitDialog();
	}
}