package com.aorta.radiobeat.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.aorta.radiobeat.R;

public class ShareDialog {
	
	public enum ShareType { TWITTER, FACEBOOK, EMAIL } 
	
	public interface ShareDialogListener {
		
		public void onClick(ShareType shareType);
	}

	private Context context;
	
	public ShareDialog(Context context) {
		
		this.context = context;
	}
	
	public void show(String title, final ShareDialogListener listener) {
		
		LayoutInflater inflater = LayoutInflater.from(context);
		
		View viewPrompt = inflater.inflate(R.layout.share, null);
		
		ImageView btnTwitter = (ImageView) viewPrompt.findViewById(R.id.share_twitter);
		ImageView btnFacebook = (ImageView) viewPrompt.findViewById(R.id.share_facebook);
		ImageView btnEmail = (ImageView) viewPrompt.findViewById(R.id.share_email);
				
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setView(viewPrompt);
		builder.setCancelable(false);
		
		builder.setNeutralButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
			}
		});
		
		final AlertDialog dialog = builder.create();
		dialog.show();
		
		btnTwitter.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {

				if (listener != null)
					listener.onClick(ShareType.TWITTER);

				dialog.dismiss();
			}
		});
		
		btnFacebook.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {

				if (listener != null)
					listener.onClick(ShareType.FACEBOOK);
				
				dialog.dismiss();
			}
		});
		
		btnEmail.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {

				if (listener != null)
					listener.onClick(ShareType.EMAIL);
				
				dialog.dismiss();
			}
		});		
	}	
}