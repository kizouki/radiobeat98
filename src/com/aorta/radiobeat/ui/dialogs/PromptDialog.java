package com.aorta.radiobeat.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.aorta.radiobeat.R;

public class PromptDialog {
	
	public interface PrompDialogListener {
		
		public void onResult(String message);
	}

	private Context context;
	
	public PromptDialog(Context context) { 
		
		this.context = context;
	}
	
	public void show(String title, String message, final PrompDialogListener listener) {
		
		LayoutInflater inflater = LayoutInflater.from(context);
		
		View viewPrompt = inflater.inflate(R.layout.prompt, null);
		
		TextView txtTitle = (TextView) viewPrompt.findViewById(R.id.prompt_txt_title);
		final EditText edtInput = (EditText) viewPrompt.findViewById(R.id.prompt_edt_input);
		
		txtTitle.setText(title);
		edtInput.setText(message);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setView(viewPrompt);
		builder.setCancelable(false);
		
		builder.setPositiveButton(context.getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (listener != null) {
					
					String message = edtInput.getText().toString();
					
					listener.onResult(message);
				}
			}
		});
		
		builder.setNegativeButton(context.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.cancel();
			}
		});
		
		builder.create().show();		
	}
}