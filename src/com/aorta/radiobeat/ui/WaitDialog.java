package com.aorta.radiobeat.ui;

import android.content.Context;
import android.os.Bundle;

import com.aorta.radiobeat.R;

public class WaitDialog extends android.app.Dialog {

	public WaitDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
		
		super(context, R.style.WaitDialog);

		setCancelable(cancelable);

		setOnCancelListener(cancelListener);
	}

	protected int getLayoutId() {
		
		return 0;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

		setContentView(getLayoutId());
	}
}