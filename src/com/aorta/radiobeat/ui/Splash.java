package com.aorta.radiobeat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;

import com.aorta.radiobeat.Constants;
import com.aorta.radiobeat.R;

@SuppressWarnings("deprecation")
public class Splash extends ActivityBase {
	
	private static final int SPLASH_DELAY = 1000 * 1;
	
	private Runnable showRadio;
	private Runnable showAdvertising;
	
	private boolean loadedAdvertising = false;	
	
	private WebView webView;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.splash);
    }
    
	@Override
	protected void onDestroy() {

		if (showAdvertising != null)
			getHandler().removeCallbacks(showAdvertising);
		
		if (showRadio != null)
			getHandler().removeCallbacks(showRadio);
		
		super.onDestroy();
	}    
    
    protected void onAppLogInResult(int error) { 
    	
		switch (error) {

			case NO_ERROR:
	
				showAdvertising();
				
				showRadio = new Runnable() {
					
					@Override
					public void run() {
	
						if (loadedAdvertising) {
							
							getHandler().postDelayed(new Runnable() {
								
								@Override
								public void run() {
	
									showRadio();
								}
								
							}, Constants.Advertising.DELAY);
							
						} else {
							
							getHandler().postDelayed(showRadio, 1000);
						}
					}
				};
				
				getHandler().postDelayed(showRadio, 1000);
	
				break;
	
			case NETWORK_ERROR:
	
				showInformation(getString(R.string.network_error), new Runnable() {
	
					@Override
					public void run() {
	
						finish();
					}
				});
	
				break;
	
			case SERVER_ERROR:
	
				showInformation(getString(R.string.server_error), new Runnable() {
	
					@Override
					public void run() {
	
						finish();
					}
				});
	
				break;
		}
    }    
    
    private void showRadio() {
    	
    	getHandler().postDelayed(new Runnable() {
			
			@Override
			public void run() {

				Intent intent = new Intent(Splash.this, Radio.class);
				
				startActivity(intent);
				
				finish();
			}
			
		}, SPLASH_DELAY);
    }

	private void showAdvertising() {
		
		webView = (WebView) findViewById(R.id.splash_webview);
		webView.setBackgroundResource(R.drawable.bg_default);
		
		webView.setWebViewClient(new LocalViewClient());
		webView.setWebChromeClient(new LocalChromeClient());
		
		webView.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webView.getSettings().setBuiltInZoomControls(true);

		disableWebViewScrolls(webView);
		
		showWaitDialog(null);

		String url = String.format(Constants.Advertising.SPLASH, Integer.toString(getNonce()));
		
		webView.loadUrl(url);
	}
	
	private void showBanner() {
		
		getHandler().post(new Runnable() {
			
			@Override
			public void run() {
				
				hideWaitDialog();
				
				webView.setVisibility(View.VISIBLE);
				
				loadedAdvertising = true;
			}
		});
	}
	
	protected class LocalChromeClient extends WebChromeClient {

		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			
			super.onProgressChanged(view, newProgress);

			if (newProgress == 100) {
				
				showBanner();
			}
		}
	}	
}