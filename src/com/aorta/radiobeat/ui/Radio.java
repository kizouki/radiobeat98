package com.aorta.radiobeat.ui;

import net.chilicat.m3u8.PlayList;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import twitter4j.TwitterException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.aorta.radiobeat.Constants;
import com.aorta.radiobeat.Constants.FontType;
import com.aorta.radiobeat.R;
import com.aorta.radiobeat.RBApplication;
import com.aorta.radiobeat.api.task.BeginChannelLogTask;
import com.aorta.radiobeat.api.task.DoLikeOrDislikeTask;
import com.aorta.radiobeat.api.task.EndChannelLogTask;
import com.aorta.radiobeat.api.task.GetChannelByIdTask;
import com.aorta.radiobeat.api.task.GetM3U8PlaylistTask;
import com.aorta.radiobeat.api.task.GetMusicCoverTask;
import com.aorta.radiobeat.api.task.GetMusicInfoTask;
import com.aorta.radiobeat.api.task.TaskBase;
import com.aorta.radiobeat.api.task.TaskResponse;
import com.aorta.radiobeat.api.task.TaskResult;
import com.aorta.radiobeat.api.task.TwitterUpdateStatusTask;
import com.aorta.radiobeat.api.task.TwitterUpdateStatusTaskResponse;
import com.aorta.radiobeat.api.to.Channel;
import com.aorta.radiobeat.api.to.MusicInfoResult;
import com.aorta.radiobeat.helper.SharedPreferencesHelper;
import com.aorta.radiobeat.helper.facebook.DialogError;
import com.aorta.radiobeat.helper.facebook.Facebook;
import com.aorta.radiobeat.helper.facebook.FacebookError;
import com.aorta.radiobeat.helper.twitter.TwDialog;
import com.aorta.radiobeat.helper.twitter.Twitter;
import com.aorta.radiobeat.helper.twitter.TwitterError;
import com.aorta.radiobeat.player.AACPlayerService;
import com.aorta.radiobeat.player.AACPlayerService.PlayerState;
import com.aorta.radiobeat.ui.dialogs.PromptDialog;
import com.aorta.radiobeat.ui.dialogs.ShareDialog;
import com.aorta.radiobeat.ui.dialogs.ShareDialog.ShareType;

@SuppressWarnings("deprecation")
public class Radio extends ActivityBase implements OnClickListener, OnSeekBarChangeListener {
	
	private static final int UPDATE_MUSIC_DELAY = 1000 * 15;
	
	private ServiceConnection conn;
	private AACPlayerService player;	
	private BroadcastReceiver playerStateChangeReceiver = new PlayerStateChangeReceiver();
	
	private ActionBar actionBar;
	
	private ImageView imgMusicCover;
	private TextView txtMusicName;
	private TextView txtMusicArtist;
	private TextView txtMusicTime;
	private ImageView btnLike;
	private ImageView btnDislike;
	private ImageView btnLyric;
	private ImageView btnShare;
	private ImageView btnPlayStop;
	private TextView txtNextMusic;
	private TextView txtNextMusicCaption;
	private SeekBar barVolume;
	
	private Channel channel;
	private PlayList playList;
	private String channelLogId = null;
	
	private AudioManager audioManager;
	
	private Facebook facebook;
	
	private WebView webViewBanner;
	
	private MusicInfoResult currentMusicInfo;
	
	private Runnable runHideBanner = new Runnable() {
		
		@Override
		public void run() {
			
			getBannerContainer().setVisibility(View.GONE);
			
			webViewBanner.clearView();
		}
	};	
	
	private Runnable updateMusic = new Runnable() {
		
		@Override
		public void run() {
			
			getMusicInfo();
		}
	};	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.radio);
		
		configureBanner();
		
		audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		
		actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	    actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.header));
	    actionBar.setDisplayShowTitleEnabled(false);
	    actionBar.setDisplayHomeAsUpEnabled(false);			
		
	    initViews();
	    
		attachToPlayerService();
	}
	
    @Override
    protected void onDestroy() {

    	try {
    		
    		stopAndLog();
    		
    		detachToPlayeService();
    		
    	} catch (Exception e) {
    		
    		e.printStackTrace();
		}
    	
    	super.onDestroy();
    }
    
    @Override
    protected void onStart() {

    	super.onStart();
    	
    	showBanner();
    }
    
    private void initViews() {
    	
		Drawable thumbVolume = getResources().getDrawable(R.drawable.bg_volume_pino);
		thumbVolume.setBounds(new Rect(0, 0, thumbVolume.getIntrinsicWidth(), thumbVolume.getIntrinsicHeight()));
    	
    	imgMusicCover = (ImageView) findViewById(R.id.radio_music_cover);
    	txtMusicName = (TextView) findViewById(R.id.radio_music_name);
    	txtMusicArtist = (TextView) findViewById(R.id.radio_music_artist);
    	txtMusicTime = (TextView) findViewById(R.id.radio_txt_music_time);
    	btnLike = (ImageView) findViewById(R.id.radio_btn_like);
    	btnDislike = (ImageView) findViewById(R.id.radio_btn_dislike);
    	btnLyric = (ImageView) findViewById(R.id.radio_btn_lyric);
    	btnShare = (ImageView) findViewById(R.id.radio_btn_share);
    	btnPlayStop = (ImageView) findViewById(R.id.radio_btn_play_stop);
    	txtNextMusic = (TextView) findViewById(R.id.radio_next_music);
    	txtNextMusicCaption = (TextView) findViewById(R.id.radio_next_music_caption);
    	barVolume = (SeekBar) findViewById(R.id.radio_volume);
    	
    	txtNextMusicCaption.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOLD_OBLIQUE));
    	txtMusicName.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOLD_OBLIQUE));
    	txtMusicArtist.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOOK));
    	txtMusicTime.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOOK));
    	txtNextMusic.setTypeface(RBApplication.getInstance().getTypeface(FontType.FUTURA_STD_BOOK));
    	  	
		int volMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		int volMed = (int) (volMax / 2);
		
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volMed, 0);
    	
    	barVolume.setOnSeekBarChangeListener(this);
    	barVolume.setMax(volMax);
    	barVolume.setProgress(volMed);
    	barVolume.setThumb(thumbVolume);
    	barVolume.setThumbOffset(0);
    	
    	btnLike.setOnClickListener(this);
    	btnDislike.setOnClickListener(this);
    	btnLyric.setOnClickListener(this);
    	btnShare.setOnClickListener(this);
    	btnPlayStop.setOnClickListener(this);
    	
    	updateMusicInfo(null);
    	updatePlayerState("");
    }
    
	private void configureBanner() {
		
		webViewBanner = (WebView) findViewById(R.id.radio_webview_banner);
		webViewBanner.setBackgroundColor(Color.parseColor("#000000"));
		
		webViewBanner.setWebViewClient(new LocalViewClient());
		webViewBanner.setWebChromeClient(new LocalChromeClient());
		
		webViewBanner.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webViewBanner.getSettings().setBuiltInZoomControls(true);
		webViewBanner.getSettings().setUseWideViewPort(false); 
		
		disableWebViewScrolls(webViewBanner);		
	}  
	
	private ViewGroup getBannerContainer() {
		
		return (ViewGroup) findViewById(R.id.radio_container_banner);
	}	
	
	private void showBanner() {
		
		getHandler().post(new Runnable() {
			
			@Override
			public void run() {
				
				getHandler().removeCallbacks(runHideBanner);
				
				getBannerContainer().setVisibility(View.VISIBLE);

				String url = String.format(Constants.Advertising.PLAYER, Integer.toString(getNonce()));
				
				webViewBanner.loadUrl(url);
			}
		});
	}	
    
	private void detachToPlayeService() {
		
		unbindService(conn);
		
		unregisterReceiver(playerStateChangeReceiver);		
	}    
	
	private void attachToPlayerService() {
		
		Intent serviceIntent = new Intent(this, AACPlayerService.class);
		
		conn = new ServiceConnection() {
			
			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				
				player = ((AACPlayerService.ListenBinder) service).getService();
				
				attachedToPlayerService();
			}

			@Override
			public void onServiceDisconnected(ComponentName name) {
				
				player = null;
			}
		};

		startService(serviceIntent);
		
		bindService(serviceIntent, conn, 0);
		
	    registerReceiver(playerStateChangeReceiver, new IntentFilter(AACPlayerService.AAC_PLAYER_STATE_CHANGE));
	}
	
	private void attachedToPlayerService() { 
		
		doPlay();
	}	
	
	private void getM3U8PlaylistAndPlay(String urlM3U8) {
		
		TaskBase task = new GetM3U8PlaylistTask(new TaskResult() {
			
			@Override
			public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {

				hideWaitDialog();
				
				if (taskResponse.getTo() != null) {
					
					playList = (PlayList) taskResponse.getTo().getTag();
					
					play();
				}
			}
		});
		
		showWaitDialog(task);
		
		task.execute(urlM3U8);
	}	
	
	private void onPlayerPlaying() {
		
		getHandler().post(new Runnable() {
			
			@Override
			public void run() {

				btnPlayStop.setImageResource(R.drawable.bt_stop);
				
				hideWaitDialog();
				
				beginChannelLog(channel);
				
				getMusicInfo();
				
				updatePlayerState("no ar.");
			}
		});
	}
	
	private void onPlayerStop(final boolean error) {
		
		getHandler().post(new Runnable() {
			
			@Override
			public void run() {
				
				hideWaitDialog();
				
				endChannelLog();
				
				updateMusicInfo(null);
				
				getHandler().removeCallbacks(updateMusic);
				
				btnPlayStop.setImageResource(R.drawable.bt_play);
				
				updatePlayerState("parado.");
				
				if (error)
					stop();
			}
		});		
	}
	
	private void doPlay() {
		
		if (channel != null && playList != null) {
			
			play();
			
		} else {
			
			getChannelById(Constants.Channel.ID);
		}
	}
	
	private void updatePlayerState(String state) {
		
		txtMusicTime.setText(state);
	}
	
	private void getChannelById(String idChannel) {
		
		TaskBase task = new GetChannelByIdTask(new TaskResult() {
			
			@Override
			public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {
				
				hideWaitDialog();

				if (taskResponse.getTo() != null) {
					
					channel = (Channel) taskResponse.getTo();

					getM3U8PlaylistAndPlay(channel.getStreamUrl());
				}
			}
		});
		
		showWaitDialog(task);
		
		task.execute(idChannel);
	}
	
	private void beginChannelLog(final Channel channel) {
		
		TaskBase task = new BeginChannelLogTask(new TaskResult() {
			
			@Override
			public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {

				if (taskResponse.getTo() != null) {
					
					channelLogId = taskResponse.getTo().getTag().toString();
				}
			}
		});
		
		task.execute(getAppId(), channel.getId());
	}
	
	private void endChannelLog() {
		
		if (channelLogId != null) {
			
			TaskBase task = new EndChannelLogTask(new TaskResult() {
				
				@Override
				public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {
					
					channelLogId = null;
				}
			});
			
			task.execute(channelLogId);			
		}
	}	
	
	private void play() {
		
		if (playList != null && player != null && !player.isPlaying()) {
			
			player.play(playList.getElements().get(0).getURI().toString());
			
			updatePlayerState("carregando...");
		}
	}
	
	private void stop() {
		
		if (player != null && player.isPlaying()) {
			
			player.stop();
			
			getHandler().removeCallbacks(updateMusic);
		}
	}	
	
	private void stopAndLog() {
		
		stop();
		
		endChannelLog();
	}
	
	private void btnLikeClick() { 
		
		doLikeOrDislike(Constants.Content.LIKE_PARAM);
	}
	
	private void btnDislikeClick() { 

		doLikeOrDislike(Constants.Content.DISLIKE_PARAM);
	}
	
	private void btnLyricClick() { 
		
		if (currentMusicInfo != null && currentMusicInfo.getMusicaCorrente() != null) {
			
			Intent intent = new Intent(this, Lyric.class);
			
			intent.putExtra("CURRENT_MUSIC_INFO_PARAM", currentMusicInfo.toString());
			
			startActivity(intent);	
			
		} else {
			
			showInformation("Sem informações da música no momento. Tente novamente mais tarde.", null);
		}
	}
	
	private void btnShareClick() { 
		
		ShareDialog dialog = new ShareDialog(this);
		
		dialog.show("Compartilhe", new ShareDialog.ShareDialogListener() {
			
			@Override
			public void onClick(ShareType shareType) {

				switch (shareType) {
				
					case TWITTER: btnTwitterClick(); break;
					
					case FACEBOOK: btnFacebookClick(); break;
					
					case EMAIL: btnEmailClick(); break;
				}
			}
		});
	}
	
	private void doLikeOrDislike(String likeOrDislike) {
		
		if (currentMusicInfo != null && currentMusicInfo.getMusicaCorrente() != null &&
			currentMusicInfo.getMusicaCorrente().getInteratividade() != null) {
			
			String pulsarId = currentMusicInfo.getMusicaCorrente().getInteratividade().getPulsarId();
			String grupoMidiaId = currentMusicInfo.getMusicaCorrente().getInteratividade().getGrupoMidiaId();
			String nomeArtista = currentMusicInfo.getMusicaCorrente().getArtista();
			String nomeMusica = currentMusicInfo.getMusicaCorrente().getTitulo();
			
			TaskBase task = new DoLikeOrDislikeTask(new TaskResult() {
				
				@Override
				public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {

					hideWaitDialog();
					
					if (taskResponse.getException() == null)
						showInformation("Obrigado pelo seu voto!", null);
					else
						showInformation("No momento não podemos computar seu voto. Tente novamente mais tarde.", null);
				}
			});
			
			showWaitDialog(task);
			
			task.execute(likeOrDislike, pulsarId, grupoMidiaId, nomeArtista, nomeMusica);	
			
		} else {
		
			showInformation("Sem informação da música. Tente novamente mais tarde", null);
		}
	}
	
	private void btnPlayStopClick() { 
		
		showWaitDialog(null);
		
		if (player != null && !player.isPlaying())
			play();
		else
			stop();
	}
	
	private void btnTwitterClick() { 
		
		if (RBApplication.getInstance().getTwitter() == null) {
			
		    CommonsHttpOAuthProvider commonsHttpOAuthProvider = new CommonsHttpOAuthProvider(
		    		com.aorta.radiobeat.helper.twitter.Constants.TWITTER_OAUTH_REQUEST_TOKEN_ENDPOINT,
		    		com.aorta.radiobeat.helper.twitter.Constants.TWITTER_OAUTH_ACCESS_TOKEN_ENDPOINT,
		    		com.aorta.radiobeat.helper.twitter.Constants.TWITTER_OAUTH_AUTHORIZE_ENDPOINT);
	        
		    CommonsHttpOAuthConsumer commonsHttpOAuthConsumer = new CommonsHttpOAuthConsumer(Constants.Twitter.CONSUMER_KEY,
		    		Constants.Twitter.CONSUMER_SECRET);
	        
	        commonsHttpOAuthProvider.setOAuth10a(true);		
			
	        TwDialog dialog = new TwDialog(this, commonsHttpOAuthProvider, commonsHttpOAuthConsumer, new Twitter.DialogListener() {

				@Override
				public void onComplete(Bundle values) {

					saveAccessToken(values.getString("access_token"), values.getString("secret_token"));
					
					getHandler().post(new Runnable() {
						
						@Override
						public void run() {

							showTwitterPrompt(getString(R.string.prompt_title), getString(R.string.twitter_message));
						}
					});
				}

				@Override
				public void onTwitterError(TwitterError e) { }

				@Override
				public void onError(com.aorta.radiobeat.helper.twitter.DialogError e) { }

				@Override
				public void onCancel() { }
	        	
	        }, R.drawable.ic_launcher);
	        
	        dialog.show();
	        
		} else {
			
			showTwitterPrompt(getString(R.string.prompt_title), getString(R.string.twitter_message));
		}
	}
	
	private void btnFacebookClick() { 
		
		if (facebook == null)
			facebook = new Facebook(Constants.Facebook.APP_KEY);
		
		Bundle params = new Bundle();			
		
		params.putString("name", getString(R.string.app_name));
		params.putString("caption", getString(R.string.facebook_message));
		params.putString("description", ""); 
		params.putString("link", "http://beat98.globoradio.globo.com/home/HOME.htm");
		
		facebookPostContentDialog(params);
	}
	
	private void btnEmailClick() { 
		
		enviarEmail(getString(R.string.app_name), getString(R.string.email_message));
	}
	
	private void facebookPostContentDialog(Bundle params) {
		
		facebook.dialog(this, "stream.publish", params, new Facebook.DialogListener() {
			
			@Override
			public void onFacebookError(FacebookError error) { 
				
				facebookDialogComplete(false);
			}
			
			@Override
			public void onError(DialogError e) { 
				
				facebookDialogComplete(false);
			}
			
			@Override
			public void onComplete(Bundle values) {
				
				final String postId = values.getString("post_id");
				
				facebookDialogComplete(postId != null);
			}
			
			@Override
			public void onCancel() { 
				
				facebookDialogComplete(false);
			}
		});
	}
	
	private void facebookDialogComplete(final boolean sucess) { 
		
		getHandler().post(new Runnable() {
			
			@Override
			public void run() {
				
				Toast.makeText(Radio.this, sucess ? getString(R.string.radio_send_facebook_ok) :
					getString(R.string.radio_send_facebook_error), Toast.LENGTH_LONG).show();
			}
		});
	}
	
	private void saveAccessToken(String token, String secret) {
		
        SharedPreferencesHelper.write(this, Constants.SharedPrefsKeys.DEFAULT_SHARED_PREFS,
				Constants.SharedPrefsKeys.TWITTER_TOKEN, token);
		
        SharedPreferencesHelper.write(this, Constants.SharedPrefsKeys.DEFAULT_SHARED_PREFS,
				Constants.SharedPrefsKeys.TWITTER_SECRET, secret);		
	}
    
    private void twitterUpdateStatus(String status) {
    	
		new TwitterUpdateStatusTask() {
			
			@Override
			protected void onPostExecute(TwitterUpdateStatusTaskResponse response) {
				
				if (response.getStatus() != null) {
					
					Toast.makeText(Radio.this, getString(R.string.radio_send_twitter_ok), Toast.LENGTH_LONG).show();
					
				} else if (response.getException() != null && (response.getException() instanceof TwitterException)) {
					
					Toast.makeText(Radio.this, ((TwitterException) response.getException()).getErrorMessage(), Toast.LENGTH_LONG).show();
					
				} else {
					
					Toast.makeText(Radio.this, getString(R.string.radio_send_twitter_error), Toast.LENGTH_LONG).show();
				}
			};
			
		}.execute(status);	   	
    }	
    
	private void showTwitterPrompt(String title, String initMessage) { 
		
		PromptDialog promptDialog = new PromptDialog(this);
		
		promptDialog.show(title, initMessage, new PromptDialog.PrompDialogListener() {
			
			@Override
			public void onResult(String message) {

				if (!TextUtils.isEmpty(message)) {
					
					twitterUpdateStatus(message);
				}
			}
		});
	}   
	
	private void enviarEmail(String subject, String text) {
		
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

		emailIntent.setType("text/html");

		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { "" });

		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);

		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);

		startActivity(Intent.createChooser(emailIntent, null));
	}
	
	private void getMusicInfo() {
		
		TaskBase task = new GetMusicInfoTask(new TaskResult() {
			
			@Override
			public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {

				if (taskResponse.getTo() != null)
					updateMusicInfo((MusicInfoResult) taskResponse.getTo());
				else
					currentMusicInfo = null;
				
				getHandler().postDelayed(updateMusic, UPDATE_MUSIC_DELAY);
			}
		});
		
		task.execute();
	}
	
	private void updateMusicInfo(MusicInfoResult info) {
		
		if (info != null && info.getMusicaCorrente() != null) {
			
			currentMusicInfo = info;
			
			String music = info.getMusicaCorrente().getTitulo();
			String artist = info.getMusicaCorrente().getArtista();
			String coverUrl = info.getMusicaCorrente().getImagem();
			
			txtMusicName.setText(!TextUtils.isEmpty(music) ? music : ""); 
			txtMusicArtist.setText(!TextUtils.isEmpty(artist) ? artist : "");	
			
			if (!TextUtils.isEmpty(coverUrl))
				getMusicCover(coverUrl);
			else
				imgMusicCover.setImageResource(R.drawable.bg_default_cover);
			
			if (info.getMusicaPosterior() != null) {
				
				music = info.getMusicaPosterior().getTitulo();
				
				txtNextMusic.setText(!TextUtils.isEmpty(music) ? music : "");
			}
			
		} else {
			
			txtMusicName.setText(""); 
			txtMusicArtist.setText("");
			txtNextMusic.setText("");
			imgMusicCover.setImageResource(R.drawable.bg_default_cover);
		}		
	}

	private void getMusicCover(String url) {
		
		TaskBase task = new GetMusicCoverTask(new TaskResult() {
			
			@Override
			public void onTaskResult(AsyncTask<?, ?, ?> task, TaskResponse taskResponse) {

				if (taskResponse.getTo() != null) {
					
					try {
						
						byte[] data = (byte[]) taskResponse.getTo().getTag();
						
						Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
						
						imgMusicCover.setImageBitmap(bitmap);
						
					} catch (Exception e) {
						
						e.printStackTrace();
						
						imgMusicCover.setImageResource(R.drawable.bg_default_cover);
					}
					
				} else {
					
					imgMusicCover.setImageResource(R.drawable.bg_default_cover);
				}
			}
		});
		
		task.execute(url);
	}
	
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		
			case R.id.radio_btn_like: btnLikeClick(); break;
			
			case R.id.radio_btn_dislike: btnDislikeClick(); break;
			
			case R.id.radio_btn_lyric: btnLyricClick(); break;
			
			case R.id.radio_btn_share: btnShareClick(); break;
			
			case R.id.radio_btn_play_stop: btnPlayStopClick(); break;
		}
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) { 
		
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) { }

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) { }	
	
	private class PlayerStateChangeReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {

			PlayerState state = PlayerState.valueOf(intent.getStringExtra(AACPlayerService.AAC_PLAYER_CURRENT_STATE));
			
			switch(state) {
			
				case ERROR: onPlayerStop(true); break;
				
				case PLAYING: onPlayerPlaying(); break;
				
				case STOPPED: onPlayerStop(false); break;
			}
		}
	}	
	
	protected class LocalChromeClient extends WebChromeClient {

		@Override
		public void onProgressChanged(WebView view, int newProgress) {
			
			super.onProgressChanged(view, newProgress);

			/*
			if (newProgress == 100) {
				
				getHandler().postDelayed(runHideBanner, Constants.Advertising.DELAY);
			}
			*/
		}
	}	
}