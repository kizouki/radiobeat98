package com.aorta.radiobeat;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import android.app.Application;
import android.graphics.Typeface;

import com.aorta.radiobeat.Constants.FontType;
import com.aorta.radiobeat.helper.SharedPreferencesHelper;

public class RBApplication extends Application {

	private static RBApplication instance;
	
	private Twitter twitter;
	
	private Typeface ftFuturaStdBook;
	private Typeface ftFuturaStdBoldOblique;
	
	@Override
	public void onCreate() {

		super.onCreate();
		
		instance = this;
		
		ftFuturaStdBook = Typeface.createFromAsset(getAssets(), "fonts/FuturaStd-Book.otf");
		ftFuturaStdBoldOblique = Typeface.createFromAsset(getAssets(), "fonts/FuturaStd-BoldOblique.otf");
	}

	public static RBApplication getInstance() {
		
		return instance;
	}	
	
	private void initTwitter() {
		
		String token = SharedPreferencesHelper.read(this, Constants.SharedPrefsKeys.DEFAULT_SHARED_PREFS,
				Constants.SharedPrefsKeys.TWITTER_TOKEN, null);
		
		String secret = SharedPreferencesHelper.read(this, Constants.SharedPrefsKeys.DEFAULT_SHARED_PREFS,
				Constants.SharedPrefsKeys.TWITTER_SECRET, null);	
		
		if (token != null && secret != null) {
			
			AccessToken accessToken = new AccessToken(token, secret);

			twitter = new TwitterFactory().getInstance();
			
			twitter.setOAuthConsumer(Constants.Twitter.CONSUMER_KEY, Constants.Twitter.CONSUMER_SECRET);
			
			twitter.setOAuthAccessToken(accessToken);			
		}	
	}
	
	public Twitter getTwitter() {
		
		if (twitter == null) {
			
			initTwitter();
		}
		
		return twitter;
	}
	
	public Typeface getTypeface(FontType fontType) {
		
		switch (fontType) {
			
			case FUTURA_STD_BOOK: return ftFuturaStdBook;
			
			case FUTURA_STD_BOLD_OBLIQUE: return ftFuturaStdBoldOblique;
			
			default: return ftFuturaStdBook;
		}
	}	
}