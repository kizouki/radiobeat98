package com.aorta.radiobeat.helper;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;

import com.aorta.radiobeat.Constants;

public class ApiHelper {
	
	private static final int TIME_OUT = 15000;

	private static Element addChildElement(Element parent, String name, String value) {

		Element child = parent.createElement(parent.getNamespace(), name);

		child.addChild(Element.TEXT, value);

		parent.addChild(Element.ELEMENT, child);

		return child;
	}
	
	private static final Element newElement(String namespace, String name) {

		Element element = new Element().createElement(namespace, name);

		return element;
	}

	private static final Element newHeader() {

		Element root = newElement(Constants.Api.DEFAULT_NAMESPACE, "CabecalhoServico");

		addChildElement(root, "VersaoAPI", "1.0");

		addChildElement(root, "IdCliente", Constants.CUSTOMER_ID);

		addChildElement(root, "OrigemAcesso", "Mobile");

		addChildElement(root, "SO", "Android");

		addChildElement(root, "Plataforma", "4");

		addChildElement(root, "Ambiente", "Prod");

		return root;
	}

	public static final Object request(String action, String url, SoapObject request) throws Exception {

		return request(action, url, request, ApiHelper.newHeader());
	}
	
	public static final Object request(String action, String url, SoapObject request, Element header) throws Exception {

		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

		soapEnvelope.dotNet = true;
		soapEnvelope.setOutputSoapObject(request);

		if (header != null) {
			
			soapEnvelope.headerOut = new Element[1];
			
			soapEnvelope.headerOut[0] = header;			
		}

		HttpTransportSE soapTransport = new HttpTransportSE(url, TIME_OUT);

		soapTransport.call(action, soapEnvelope);

		Object response = soapEnvelope.getResponse();

		return response;
	}	
	
	public static final String formatImageUrl(int w, int h, int quality, String type, String fileId) {

		if (w < 0)
			w = 0;

		if (h < 0)
			h = 0;

		String url = String.format(Constants.Api.IMAGE_DOWNLOAD_URL, w, h, quality, type, fileId);

		return url;
	}
	
	public static final String formatImageUrl(int w, int h, String type, String fileId) {

		return formatImageUrl(w, h, 80, type, fileId);
	}
}