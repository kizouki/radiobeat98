package com.aorta.radiobeat.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.os.Process;

import com.aorta.radiobeat.Constants;

public final class IoHelper {

	private static final int DEFAULT_BUFFER_SIZE = 8092;

	private static int mCachePrefixId = Process.myPid();

	public final static void clearCacheFiles(final Context context) {
		new Thread(new Runnable() {
			public void run() {
				try {
					String currentPrefix = getPhysicalCacheFilePrefix(context);
					for (File file : context.getApplicationContext().getCacheDir().listFiles()) {
						try {
							if (file.getName().startsWith(Constants.Cache.CACHE_FILES_PREFIX) && !file.getName().startsWith(currentPrefix)) {
								file.delete();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public static final File getPhysicalCacheFilePath(Context context, String fileName) {

		String physicalCacheFileName =
				context.getApplicationContext().getCacheDir().getAbsolutePath() + File.separator
						+ getPhysicalCacheFilePrefix(context)
						+ fileName;

		File file = new File(physicalCacheFileName);

		return file;
	}

	public static final String getPhysicalCacheFilePrefix(Context context) {

		String physicalCacheFilePrefix =
				Constants.Cache.CACHE_FILES_PREFIX + "_"
						+ mCachePrefixId
						+ "_"
						+ context.getApplicationContext().hashCode()
						+ "_";

		return physicalCacheFilePrefix;
	}

	public static final byte[] readAll(InputStream is) throws Exception {

		if (is == null) {

			return null;
		}

		ByteArrayOutputStream baos = null;

		try {

			baos = new ByteArrayOutputStream();

			byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];

			int size;

			while ((size = is.read(buffer, 0, buffer.length)) > 0) {

				baos.write(buffer, 0, size);
			}

			buffer = null;

			byte[] data = baos.toByteArray();

			if (data == null || data.length == 0) {

				return null;

			} else {

				return data;
			}

		} finally {

			try {

				if (baos != null) {

					baos.close();
				}

			} finally {

				baos = null;
			}
		}
	}

	public final static byte[] readCacheFileInBytes(Context context, String fileName) {

		File file = getPhysicalCacheFilePath(context, fileName);

		return readFile(file);
	}
	
	public final static File readCacheFile(Context context, String fileName) {

		return getPhysicalCacheFilePath(context, fileName);
	}	

	public final static byte[] readFile(Context context, String fileName) {

		InputStream is = null;

		try {

			is = context.getApplicationContext().openFileInput(fileName);

			return IoHelper.readAll(is);

		} catch (FileNotFoundException e) {

			return null;

		} catch (Exception e) {

			e.printStackTrace();

			return null;

		} finally {

			if (is != null) {

				try {

					is.close();

				} catch (Exception e) {

					e.printStackTrace();

				} finally {

					is = null;
				}
			}
		}
	}

	public final static byte[] readFile(File file) {

		InputStream is = null;

		try {

			is = new FileInputStream(file);

			return IoHelper.readAll(is);

		} catch (Exception e) {

			return null;

		} finally {

			if (is != null) {

				try {

					is.close();

				} catch (Exception e) {

					e.printStackTrace();

				} finally {

					is = null;
				}
			}
		}
	}

	public static final String readTextFileFromAssets(Context context, String assetName) {
		InputStream is = null;
		try {
			is = context.getApplicationContext().getAssets().open(assetName);
			return new String(IoHelper.readAll(is));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					is = null;
				}
			}
		}
	}

	public static final String readTextFileFromRawResource(Context context, int resId) {
		InputStream is = null;
		try {
			is = context.getApplicationContext().getResources().openRawResource(resId);
			return new String(IoHelper.readAll(is));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					is = null;
				}
			}
		}
	}

	public static final void resetCachePrefixId() {
		mCachePrefixId++;
	}

	public static final boolean writeCacheFile(Context context, String fileName, byte[] data) {

		File file = getPhysicalCacheFilePath(context, fileName);

		return writeFile(file, data);
	}

	public static final boolean writeFile(File file, byte[] data) {

		boolean error = false;

		boolean result = false;

		try {

			FileOutputStream fos = null;

			try {

				fos = new FileOutputStream(file);

				fos.write(data);

			} catch (Exception e) {

				error = true;

				throw e;

			} finally {

				if (fos != null) {

					try {

						fos.close();

						result = !error;

					} catch (IOException e) {

						e.printStackTrace();

					} finally {

						fos = null;
					}
				}
			}

			return result;

		} catch (Exception e) {

			e.printStackTrace();

			return false;
		}
	}

	public final static boolean writePrivateFile(Context context, String fileName, byte[] data) {

		boolean error = false;

		boolean result = false;

		try {

			FileOutputStream fos = null;

			try {

				fos =
						context.getApplicationContext().openFileOutput(	fileName,
																		Context.MODE_PRIVATE);

				fos.write(data);

			} catch (Exception e) {

				error = true;

				throw e;

			} finally {

				if (fos != null) {

					try {

						fos.close();

						result = !error;

					} catch (IOException e) {

						e.printStackTrace();

					} finally {

						fos = null;
					}
				}
			}

			return result;

		} catch (Exception e) {

			e.printStackTrace();

			return false;
		}
	}
}