package com.aorta.radiobeat.helper.twitter;

public final class Constants {
	
    public static final String TWITTER_OAUTH_REQUEST_TOKEN_ENDPOINT = "http://api.twitter.com/oauth/request_token";
    
    public static final String TWITTER_OAUTH_ACCESS_TOKEN_ENDPOINT = "http://api.twitter.com/oauth/access_token";
    
    public static final String TWITTER_OAUTH_AUTHORIZE_ENDPOINT = "http://api.twitter.com/oauth/authorize";
}