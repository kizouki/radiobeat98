package com.aorta.radiobeat.helper;

import java.net.URL;
import java.net.URLConnection;

public final class HttpHelper {

	public final static byte[] request(String url) {
		
		byte[] data = null;

		try {

			URL u = new URL(url);

			URLConnection urlConnection = u.openConnection();

			data = IoHelper.readAll(urlConnection.getInputStream());
			
		} catch (Exception e) {
			data = null;
			e.printStackTrace();
		}
		
		return data;		
	}
}